﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Common
{
	class CmdWrapper
    {
		public static List<string> cmd(string command, string args = null, string wkDir = ".\\")
		{
			var result = new List<string>();
			Process p = new Process();

			if (!File.Exists(command))
			{
				throw new FileNotFoundException();
			}

			p.StartInfo.FileName = command;
			
			if (args != null)
				p.StartInfo.Arguments = args;

			p.StartInfo.CreateNoWindow = true;
			p.StartInfo.UseShellExecute = false;
			p.StartInfo.RedirectStandardError = true;
			p.StartInfo.RedirectStandardOutput = true;
			p.StartInfo.WorkingDirectory = wkDir;
			p.Start();

			List<String> stdout = ParseOutput(p.StandardOutput.ReadToEnd());
			List<String> stderr = ParseOutput(p.StandardError.ReadToEnd());

			if (stderr.Count > 0)
				result = stderr;
			else
				result = stdout;

			p.WaitForExit();
			p.Close();

			return result;
		}

		public static Process StartCmd(string command, string args, Action<object, DataReceivedEventArgs> OutHandler = null, string wkDir = ".\\")
		{
			Process p = new Process();

			p.StartInfo.FileName = command;
			p.StartInfo.Arguments = args;
			p.StartInfo.CreateNoWindow = true;
			p.StartInfo.UseShellExecute = false;
			p.StartInfo.RedirectStandardError = true;
			p.StartInfo.RedirectStandardOutput = true;
			p.StartInfo.WorkingDirectory = wkDir;

			if (OutHandler != null)
				p.OutputDataReceived += new DataReceivedEventHandler(OutHandler);

			p.Start();
			p.BeginOutputReadLine();

			return p;
		}

		public static void StopCmd(Process p)
		{
			p.Kill();
		}

		private static List<string> ParseOutput(string str)
		{
			if (str == null)
				return null;

			char[] delimiters = { '\n', '\r' };
			var result = new List<string>();
			string[] words = str.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

			foreach(string word in words)
				result.Add(word);

			return result;
		}
	}
}
