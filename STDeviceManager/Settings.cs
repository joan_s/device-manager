﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace STDeviceManager
{
	public static class Settings
	{
		//>----------------------------------------------------------------------------------------< SETTINGS HANDLER >
		public static bool ADBAskForPath()
		{
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog()
			{
				Description = "Please select your Android SDK platform-tools folder which contains adb.exe.",
				ShowNewFolderButton = false
			};

			DialogResult result = folderBrowserDialog.ShowDialog();
			if (result == System.Windows.Forms.DialogResult.OK)
			{
				if (!ADBPathUpdate(folderBrowserDialog.SelectedPath + @"\"))
				{
					System.Windows.MessageBox.Show("Invalid Path.", "Error");
					return false;
				}
				else
				{
					Thread t = new Thread(() =>
					{
						STDAApplicationContext.CurrentContext.Daemon.ReInit();
					});
					t.Start();
					return true;
				}
			}

			return false;
		}
		public static bool ADBSetPath(string path)
		{
			if (!ADBPathUpdate(path))
			{
				System.Windows.MessageBox.Show("Invalid Path.", "Error");
				return false;
			}
			else
			{
				Thread t = new Thread(() =>
				{
					STDAApplicationContext.CurrentContext.Daemon.ReInit();
				});
				t.Start();
				return true;
			}
		}
		public static bool ADBConfiguration()
		{
			UCSetup setup = new UCSetup();
			WindowMain window = new WindowMain(setup);
			window.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
			window.ShowDialog();

			return setup.Success;
		}
		public static bool SetChanges(string newSSrange, string newSSid, string newdisplayname)
		{
			if (!SSRangeUpdate(newSSrange))
			{
				System.Windows.MessageBox.Show("Invalid Range.", "Error");
				return false;
			}

			if (!SSIDUpdate(newSSid))
			{
				System.Windows.MessageBox.Show("Invalid Spreadsheet ID.", "Error");
				return false;
			}

			if (!DisplayNameUpdate(newdisplayname))
			{
				System.Windows.MessageBox.Show("Invalid Display Name.", "Error");
				return false;
			}

			Thread t = new Thread(() =>
			{
				STDAApplicationContext.CurrentContext.Daemon.ReInit();
			});
			t.Start();

			return true;
		}

		//>----------------------------------------------------------------------------------------< METHODS >
		private static bool SSIDUpdate(string newSSID)
		{
			var rule = new Regex(@"^([a-zA-Z0-9-_]+)$");
			if (rule.IsMatch(newSSID))
			{
				Properties.Settings.Default.SpreadsheetID = newSSID;
				Properties.Settings.Default.Save();
				return true;
			}
			else
			{
				return false;
			}
		}
		private static bool SSRangeUpdate(string newSSRange)
		{
			var rule = new Regex(@"^\'(.*?)\'\!([A-Z]+(?:[0-9]+)?)\:([A-Z]+(?:[0-9]+)?)$");
			if (rule.IsMatch(newSSRange))
			{
				Properties.Settings.Default.SheetRange = newSSRange;
				Properties.Settings.Default.Save();
				return true;
			}
			else
			{
				return false;
			}
		}
		private static bool DisplayNameUpdate(string newdisplayname)
		{
			Properties.Settings.Default.DisplayUserName = newdisplayname;
			Properties.Settings.Default.Save();
			return true;
		}
		private static bool ADBPathUpdate(string path)
		{
			Console.WriteLine(path);
			if (File.Exists(Path.Combine(path, Properties.Settings.Default.ADB)))
			{
				Properties.Settings.Default.ADBLocation = path + "\\";
				Properties.Settings.Default.Save();
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
