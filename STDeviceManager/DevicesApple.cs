﻿using System;
using System.Collections.Generic;
using System.IO;
using Common;
using Utilities;

namespace Apple
{
	struct Libidevice
	{
		private static string _libpath;
		private static string _deviceinfo;
		private static string _device_id;

		public static string DeviceInfo { get { return Path.Combine(_libpath, _deviceinfo); } set { } }
		public static string DeviceID { get { return Path.Combine(_libpath, _device_id); } set { } }

		static Libidevice()
		{
			_libpath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, STDeviceManager.Properties.Settings.Default.LibiLocation);
			_deviceinfo = STDeviceManager.Properties.Settings.Default.ideviceinfo;
			_device_id = STDeviceManager.Properties.Settings.Default.idevice_id;
		}
	}

	static class AppleIO
	{
		public static string _devicelist = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, STDeviceManager.Properties.Settings.Default.AppleDeviceList);
		public static int GetDevices(List<Device> DevicesList)
		{
			int result = 0;
			List<string> idList = CmdWrapper.cmd(Libidevice.DeviceID, "-l");

			if (idList.Count > 0)
			{
				foreach (string id in idList)
				{
					if ((id != null) && (!id.Contains("ERROR")))
					{
						List<string> r = CmdWrapper.cmd(Libidevice.DeviceInfo, "-s " + id );
						if (r.Count > 0)
						{
							if ((!r[0].Contains("ERROR")) && (!r[0].Contains("Usage")))
							{
								DevicesList.Add(new AppleDevice(id));
								result++;
							}
						}
					}
				}
			}

			return result;
		}
	}

	class AppleDevice : Device
	{
		public AppleDevice(string id) : base(id)
		{
		}

		public override int UpdateDeviceInfos()
		{
			try
			{
				_Name = GetDeviceName();
				_Model = GetDeviceModel();
				_MarketName = GetMarketingName();
				_Manufacturer = GetManufacturer();
				_OS = "iOS";
				_OSVersion = GetOSVersion();
				_UDID = GetUDID();

			}
			catch
			{
				if (this.IsConnected())
					Console.WriteLine("#ERR: Unknow Error.");

				else
					Console.WriteLine("#ERR: Device disconnected.");

				return -1;
			}
			_Model = GetDeviceModel();

			return 0;
		}

		public override string GetDeviceName()
		{
			return null;
		}
		public override string GetDeviceModel()
		{
			string result = null;
			List<string> cmdOut = CmdWrapper.cmd(Libidevice.DeviceInfo, "-u " + _ID + " -s -k ProductType");
			foreach (string word in cmdOut)
			{
				result = word;
			}

			return result;
		}
		public override string GetMarketingName()
		{
			string result = null;
			try
			{
				result = Util.ParseFile(AppleIO._devicelist, _Model, ';')[0];
			}
			catch
			{
				result = "#ERR: Cannot find marketing name.";
			}
			return result;
		}
		public override string GetManufacturer()
		{
			return "Apple";
		}
		public override string GetOSVersion()
		{
			string result = null;
			List<string> cmdOut = CmdWrapper.cmd(Libidevice.DeviceInfo, "-u " + _ID + " -s -k ProductVersion");
			foreach (string word in cmdOut)
			{
				result = word;
			}

			return result;
		}
		public override string GetUDID()
		{
			return this._ID;
		}

		public override Battery GetBattery()
		{
			throw new NotImplementedException();
		}

		public override bool IsConnected()
		{
			List<string> idList = CmdWrapper.cmd(Libidevice.DeviceID, "-l");
			if (idList.Count > 0)
			{
				foreach (string id in idList)
				{
					if (id == this._ID)
					{
						return true;
					}
				}
			}
			return false;
		}
	}
}