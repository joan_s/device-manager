﻿using System.Diagnostics;

namespace Common
{
	public abstract class Device
    {
		//>------------------------------------------------------------------------------------------------------------< Constructor >
		protected Device(string id)
		{
			_ID = id;
			UpdateDeviceInfos();
		}

		//>------------------------------------------------------------------------------------------------------------< Local variables >
		public string _ID { get; protected set; }
		public string _State { get; protected set; } 

		public bool _Monitored { get; protected set; }

		public string _Name { get; protected set; }
		public string _OS { get; protected set; }
		public string _OSVersion { get; protected set; }
		public string _Model { get; protected set; }
		public string _MarketName { get; protected set; }
		public string _Manufacturer { get; protected set; }
		public string _UDID { get; protected set; }

		public Battery _battery { get; protected set; }
		public Network _network { get; protected set; }
		public App _FocusedApp { get; protected set; }

		//>------------------------------------------------------------------------------------------------------------< Methods >
		public abstract int UpdateDeviceInfos();

		public abstract bool IsConnected();

		public abstract string GetDeviceName();
		public abstract string GetDeviceModel();
		public abstract string GetUDID();
		public abstract string GetMarketingName();
		public abstract string GetManufacturer();
		public abstract string GetOSVersion();
		public abstract Battery GetBattery();
    }

	//>----------------------------------------------------------------------------------------------------------------< SubObjects >
	public class App
	{
		public string _Package { get; set; }
		public int _PID { get; set; }
		public string _MemUsage { get; set; }
		public float _CPUUsage { get; set; }
	}
	public class Network
	{
		public string _IPAdress { get; set; }
		public string _MacAdress { get; set; }
	}
	public class Battery
	{
		public bool _ACpowered { get; set; }
		public bool _USBpowered { get; set; }
		public bool _wirelesspowered { get; set; }
		public int _maxchargingcurrent { get; set; }
		public int _maxchargingvoltage { get; set; }
		public int _chargecounter { get; set; }
		public int _status { get; set; }
		public int _health { get; set; }
		public bool _present { get; set; }
		public int _level { get; set; }
		public int _scale { get; set; }
		public int _voltage { get; set; }
		public int _temperature { get; set; }
		public string _technology { get; set; }
	}
	public class Record
	{
		public string _filename { get; set; }
		public string _directory { get; set; }

		public Process _p { get; set; }
	}
}
