﻿using System;
using System.Windows.Forms;

namespace STDeviceManager
{
	/// <summary>
	/// The main entry point for the application.
	/// </summary>
	static class Program
	{
		[STAThread]
		static void Main()
		{
			bool createdNew = false;

			// Get the name of the mutex to detect if an application instance is already running
			string mutexName = System.Reflection.Assembly.GetExecutingAssembly().GetType().GUID.ToString();
			using (System.Threading.Mutex mutex = new System.Threading.Mutex(false, mutexName, out createdNew))
			{
				// Only allow one application instance to run simultaneously
				if (!createdNew)
				{
					return;
				}

				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);
				try
				{
					STDAApplicationContext context = new STDAApplicationContext();
					Application.Run(context);
				}
				catch (Exception e)
				{
					MessageBox.Show(e.Message, "Error");
				}
			}
		}
	}
}
