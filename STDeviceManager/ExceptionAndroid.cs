﻿using System;

namespace Android
{
	public class AdbNotStartedException: Exception
	{
		public AdbNotStartedException(): base(String.Format("#ERR: Adb daemon has not been started"))
		{
		}
	}
	public class AdbNotFoundException: Exception
	{
		public AdbNotFoundException(): base(String.Format("#ERR: Adb.exe not found"))
		{
		}
	}
	public class AdbUnknowStateException: Exception
	{
		public AdbUnknowStateException(): base(String.Format("#WRN: Unknow adb daemon state"))
		{
		}
	}
	public class AdbErrorException : Exception
	{
		public AdbErrorException(string error) : base(error)
		{
		}
	}
}