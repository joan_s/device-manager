﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Common;
using Utilities;

namespace Android
{
	static class AndroidDebugBridge
	{
		public static string _adb;
		public static string _devicelist;

		public static bool Check()
		{
			if (_adb == null)
			{
				_adb = Path.Combine(STDeviceManager.Properties.Settings.Default.ADBLocation, STDeviceManager.Properties.Settings.Default.ADB);
			}

			List<string> result = CmdWrapper.cmd(_adb);
			if (result[0].Contains("Android Debug Bridge"))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public static void ReinitLocation()
		{
			_adb = Path.Combine(STDeviceManager.Properties.Settings.Default.ADBLocation, STDeviceManager.Properties.Settings.Default.ADB);
			_devicelist = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, STDeviceManager.Properties.Settings.Default.AndroidDeviceList);
		}

		//>-------------------------------<ADBRESTARTSERVER>------------------------------------<//
		// Kill, then restart adb.
		// Throw :
		// - AdbNotFoundException: adb.exe not found.
		// - AdbNotStartedException: adb daemon has not been started.
		// - AdbUnknowStateException: No return catch from adb, don't know if the daemon is started
		public static void RestartServer()
		{
			ReinitLocation();

			if (_adb != null)
			{
				// Check if ADB is found
				List<String> cmdresult = CmdWrapper.cmd(_adb);
				if (cmdresult.Count > 0)
				{
					if (cmdresult[0].Contains("Android Debug Bridge"))
					{
						// Kill current adb daemon instance
						CmdWrapper.cmd(_adb, "kill-server");

						// Start a new adb daemon instance
						cmdresult = CmdWrapper.cmd(_adb, "start-server");
						if (cmdresult.Count > 0)
						{
							if (cmdresult[0].Contains("error"))
							{
								throw new AdbErrorException(cmdresult[0]);
							}
							else if (!cmdresult[1].Contains("daemon started successfully"))
							{ 
								throw new AdbNotStartedException();
							}
						}
						else
						{
							throw new AdbUnknowStateException();
						}
					}
					else
					{
						throw new AdbNotFoundException();
					}
				}
				else
				{
					throw new Exception();
				}
			}
		}

		//>----------------------------------<GETDEVICES>---------------------------------------<//
		// Fill a given List with connected Android devices. Return the number of devices found.
		public static int GetDevices(List<Device> DevicesList)
		{
			int result = 0;
			List<string> idList = CmdWrapper.cmd(_adb, "devices");
			idList.RemoveAt(0);

			if (idList.Count > 0)
			{
				foreach (string id in idList)
				{
					string[] s = id.Split('	');

					if (s[1] != "device")
						Console.WriteLine("#ERR: Device is in " + s[1] + " state.");
					else
					{
						DevicesList.Add(new AndroidDevice(s[0]));
						result++;
					}
				}
			}

			return result;
		}
	}

	class AndroidDevice : Device
	{
		public AndroidDevice(string id) : base(id)
		{
		}

		public override int UpdateDeviceInfos()
		{
			try
			{
				_Name = GetDeviceName();
				_Model = GetDeviceModel();
				_MarketName = GetMarketingName();
				_Manufacturer = GetManufacturer();
				_OS = "Android";
				_OSVersion = GetOSVersion();
				_UDID = GetUDID();

				return 0;
			}
			catch
			{
				if (this.IsConnected())
					Console.WriteLine("#ERR: Unknow Error.");
				
				else
					Console.WriteLine("#ERR: Device disconnected.");
				
				return -1;
			}
		}
		public override bool IsConnected()
		{
			bool result = false;
			List<string> idList = CmdWrapper.cmd(AndroidDebugBridge._adb, "devices");
			if (idList.Count > 0)
			{
				idList.RemoveAt(0);
				foreach (string id in idList)
				{
					string[] s = id.Split(' ');
					if (s[0] == this._ID)
					{
						if (s[1] == "device")
							result = true;
						else
							result = false;
					}
				}
			}
			return result;
		}

		public override string GetDeviceName()
		{
			string result = null;
			List<string> cmdOut = CmdWrapper.cmd(AndroidDebugBridge._adb, "-s " + _ID + " shell getprop ro.product.name");
			if (cmdOut.Count() > 0)
			{
				if (cmdOut[0].Contains("error:"))
					return null;

				foreach (string word in cmdOut)
				{
					result += word;
					result += ' ';
				}
			}
			return result;
		}
		public override string GetDeviceModel()
		{
			string result = null;
			List<string> cmdOut = CmdWrapper.cmd(AndroidDebugBridge._adb, "-s " + _ID + " shell getprop ro.product.model");
			if (cmdOut.Count() > 0)
			{
				if (cmdOut[0].Contains("error:"))
					return null;

				foreach (string word in cmdOut)
				{
					result += word;
				}
			}
			return result;
		}
		public override string GetUDID()
		{
			string result = null;
			List<string> cmdOut = CmdWrapper.cmd(AndroidDebugBridge._adb, "-s " + _ID + " shell settings get secure android_id");
			if (cmdOut.Count() > 0)
			{
				if (cmdOut[0].Contains("error:"))
					return null;

				foreach (string word in cmdOut)
				{
					result += word;
				}
			}
			return result;
		}
		public override string GetMarketingName()
		{
			string result = null;
			try
			{
				result = Util.ParseFile(AndroidDebugBridge._devicelist, _Model, ',')[1];
			}
			catch
			{
				result = "#ERR: Cannot find marketing name.";
			}
			return result;
		}
		public override string GetManufacturer()
		{
			string result = null;
			try
			{
				result = Util.ParseFile(AndroidDebugBridge._devicelist, _Model, ',')[0];
			}
			catch
			{
				result = "#ERR: Cannot find manufacturer.";
			}
			return result;
		}
		public override string GetOSVersion()
		{
			string result = null;
			List<string> cmdOut = CmdWrapper.cmd(AndroidDebugBridge._adb, "-s " + _ID + " shell getprop ro.build.version.release");
			if (cmdOut.Count() > 0)
			{
				foreach (string word in cmdOut)
				{
					result += word;
					result += ' ';
				}
			}
			return result;
		}
		public override Battery GetBattery()
		{
			Battery result = new Battery();
			char[] delimiters = { ':' };
			List<string> cmdOut = CmdWrapper.cmd(AndroidDebugBridge._adb, "-s " + _ID + " shell dumpsys battery");
			cmdOut.RemoveAt(0);

			foreach (string line in cmdOut)
			{
				if (line.Contains("AC powered"))
					result._ACpowered = Util.ToBoolean(line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[1]);
				else if (line.Contains("USB powered"))
					result._USBpowered = Util.ToBoolean(line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[1]);
				else if (line.Contains("Wireless powered"))
					result._wirelesspowered = Util.ToBoolean(line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[1]);
				else if (line.Contains("Max charging current"))
					result._maxchargingcurrent = Convert.ToInt32(line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[1]);
				else if (line.Contains("Max charging voltage"))
					result._maxchargingvoltage = Convert.ToInt32(line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[1]);
				else if (line.Contains("Charge counter"))
					result._chargecounter = Convert.ToInt32(line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[1]);
				else if (line.Contains("status"))
					result._status = Convert.ToInt32(line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[1]);
				else if (line.Contains("health"))
					result._health = Convert.ToInt32(line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[1]);
				else if (line.Contains("present"))
					result._present = Util.ToBoolean(line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[1]);
				else if (line.Contains("level"))
					result._level = Convert.ToInt32(line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[1]);
				else if (line.Contains("scale"))
					result._scale = Convert.ToInt32(line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[1]);
				else if (line.Contains("voltage"))
					result._voltage = Convert.ToInt32(line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[1]);
				else if (line.Contains("temperature"))
					result._temperature = Convert.ToInt32(line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[1]);
				else if (line.Contains("technology"))
					result._technology = line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[1];
			}
			return result;
		}

		// Android specifics
		public App GetFocusedApp()
		{
			App result = new App();

			// Get Package name
			List<string> cmdOut = CmdWrapper.cmd(AndroidDebugBridge._adb, "-s " + _ID + " shell dumpsys window windows");
			foreach (string line in cmdOut)
			{
				if (line.Contains("mFocusedApp"))
				{
					char[] delimiters = { ' ', '/' };
					result._Package = line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[4];
				}
			}
			cmdOut.Clear();

			// Get Memory usage infos
			cmdOut = CmdWrapper.cmd(AndroidDebugBridge._adb, "-s " + _ID + " shell dumpsys meminfo");
			foreach (string line in cmdOut)
			{
				if (line.Contains(result._Package))
				{
					char[] delimiters = { ' ', ':' };
					result._MemUsage = line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[0];
				}
			}
			cmdOut.Clear();

			// Get CPU usage infos
			cmdOut = CmdWrapper.cmd(AndroidDebugBridge._adb, "-s " + _ID + " shell dumpsys cpuinfo");
			foreach (string line in cmdOut)
			{
				if (line.Contains(result._Package))
				{
					char[] delimiters = { ' ', '%' };
					result._CPUUsage = Convert.ToSingle(line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[0]);
				}
			}
			cmdOut.Clear();

			// Get PID
			cmdOut = CmdWrapper.cmd(AndroidDebugBridge._adb, "-s " + _ID + " shell ps");
			foreach (string line in cmdOut)
			{
				if (line.Contains(result._Package))
				{
					char[] delimiters = { ' ' };
					result._PID = Convert.ToInt32(line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[1]);
				}
			}
			cmdOut.Clear();

			return result;
		}
		public int GetAPILevel()
		{
			int result = 0;
			List<string> cmdOut = CmdWrapper.cmd(AndroidDebugBridge._adb, "-s " + _ID + " shell getprop ro.build.version.sdk");
			if (cmdOut.Count() > 0)
			{
				if (cmdOut[0].Contains("error:"))
					return result;

				foreach (string word in cmdOut)
				{
					result = Int32.Parse(word);
				}
			}
			return result;
		}
		public int ElevateToRoot()
		{
			List<string> cmdresult = CmdWrapper.cmd(AndroidDebugBridge._adb, "-s "+ _ID + " root");
			if (cmdresult.Count <= 0)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
	}
}