﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Utilities
{
	static class Util
	{

		public static bool ToBoolean(this string str)
		{
			switch(str.ToLower())
			{
				case "true":
					return true;
				case " true":
					return true;
				case "t":
					return true;
				case "1":
					return true;
				case "false":
					return false;
				case " false":
					return false;
				case "f":
					return false;
				case "0":
					return false;

				default:
					throw new InvalidCastException("Incorrect value");
			}
		}

		public static List<string> ParseFile(string pathtofile, string tosearch, char delimiter)
		{
			List<string> result = new List<string>();

			if ((pathtofile != null) && (tosearch != null))
			{
				foreach (var line in File.ReadAllLines(pathtofile))
				{
					if (line.Contains(tosearch))
					{
						string[] array = line.Split(delimiter);
						foreach (string word in array)
							result.Add(word);
					}
				}
			}
			else
				throw new ArgumentNullException();

			return result;
		}
	}
}

