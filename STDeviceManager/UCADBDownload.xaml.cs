﻿using System;
using System.IO;
using System.ComponentModel;
using System.Net;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows;

namespace STDeviceManager
{
	/// <summary>
	/// Logique d'interaction pour UCADBDownload.xaml
	/// </summary>
	public partial class UCADBDownload : UserControl
	{
		public UCADBDownload(UCSetup setup)
		{
			InitializeComponent();
			_setup = setup;

			DownloadADB();
		}

		private UCSetup _setup;

		private void DownloadADB()
		{
			if (!Directory.Exists(_setup.InstallPath))
			{
				Directory.CreateDirectory(_setup.InstallPath);
			}

			using (WebClient client = new WebClient())
			{
				client.DownloadProgressChanged += DownloadProgressChanged;
				client.DownloadFileCompleted += DownloadFileCompleted;
				try
				{
					client.DownloadFileAsync(new System.Uri(Properties.Settings.Default.ADBURL), _setup.InstallPath + @"\adb.zip");
				}
				catch(Exception e)
				{
					System.Windows.MessageBox.Show(e.Message, "Error");
					_setup.Success = false;
				}
			}
		}
		private void DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		{
			progressBar.Value = e.ProgressPercentage;
			progressPercent.Text = e.ProgressPercentage + "%";
		}
		private void DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
		{
			_setup.Downloaded = true;
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			(Resources["SubContentFadeEffect"] as Storyboard).Begin();
		}
	}
}
