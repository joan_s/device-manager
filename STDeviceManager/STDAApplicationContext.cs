﻿using System.Windows.Forms;

namespace STDeviceManager
{
	class STDAApplicationContext : ApplicationContext
	{
		public STDAApplicationContext()
		{
			if (_currentContext == null)
			{
				_currentContext = this;
			}

			_daemon = new Daemon();
			_notifyIcon = new SysTrayIcon();

			if(_daemon.ADBValidation())
			{
				_daemon.IsStarted = true;
			}
			else
			{
				if(Settings.ADBConfiguration())
				{
					_daemon.IsStarted = true;
				}
			}
		}

		//>----------------------------------------------------------------------------------------------< APPLICATION CONTEXT >
		private static STDAApplicationContext _currentContext;

		//> PROPERTY
		public static STDAApplicationContext CurrentContext
		{
			get
			{
				return _currentContext;
			}
		}

		public void Exit()
		{
			_notifyIcon.Visible = false;
			if (_daemon != null)
			{
				_daemon.IsStarted = false;
				_daemon = null;
			}
			Application.Exit();
		}

		//>----------------------------------------------------------------------------------------------< SYSTRAY ICON >
		private SysTrayIcon _notifyIcon = null;

		//> PROPERTY
		public SysTrayIcon NotifyIcon
		{
			get
			{
				return _notifyIcon;
			}
			set
			{
				_notifyIcon = value;
			}
		}

		//>----------------------------------------------------------------------------------------------< DAEMON >
		private Daemon _daemon = null;

		//> PROPERTY
		public Daemon Daemon
		{
			get
			{
				return _daemon;
			}
			set
			{
				_daemon = value;
			}
		}
	}
}
