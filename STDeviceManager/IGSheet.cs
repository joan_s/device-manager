﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Text.RegularExpressions;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;
using Newtonsoft.Json;

namespace Google
{
	/* 
	 * >--< USAGE >--------------------------------------------------------------------------------
	 * 
	 *  See https://developers.google.com/sheets/api/quickstart/dotnet for more details about
	 *  Google Sheets API and how to setup your project.
	 *  
	 *  1. Generate an OAuth client ID for your project in the google developer console.
	 *		Direct link to projects wizard: https://goo.gl/fBPJ2Y
	 *  2. Download it as JSON file, and rename the file client_secret.json.
	 *  3. Move this file in your working directory.
	 *  4. Add it to your VS solution, and set his Copy to Output Directory property to 
	 *		Always Copy.
	 *	5. You're ready to go! 
	 *  
	 *  To use this object you need to instantiate it with the following parameters:
	 *		>	_spreadsheetID: You're targeted Google sheet ID as string.
	 *			See https://developers.google.com/sheets/api/guides/concepts#spreadsheet_id
	 *		
	 *		>	_applicationName: You're project name which going to be displayed on the 
	 *			application authorization request.
	 *			
	 *	By default, read and write right are asked. If you want to restrict the scope, replace this
	 *	line:
	 *				private string[] _scopes = { SheetsService.Scope.Spreadsheets };
	 *	with: 
	 *				private string[] _scopes = { SheetsService.Scope.Spreadsheets.readonly };
	 *	
	 * >------------------------------------------------------------------------------------------ 
	 */

	class IGSheet
	{
		private string[] _scopes = { SheetsService.Scope.Spreadsheets };
		private string _applicationName;
		private string _spreadsheetID;

		public IGSheet(string sheetId, string appName)
		{
			_spreadsheetID = sheetId;
			_applicationName = appName;
		}

		private UserCredential GetCredential()
		{
			UserCredential credential;

			// Generate & save credential files
			using (var stream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "client_secret.json", FileMode.Open, FileAccess.Read))
			{
				string credPath = System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
				credPath = Path.Combine(credPath, ".credentials/sheets.googleapis.com-dotnet-DeviceManager.json");

				CancellationTokenSource cts = new CancellationTokenSource();
				cts.CancelAfter(TimeSpan.FromSeconds(20));
				CancellationToken ct = cts.Token;

				credential = GoogleWebAuthorizationBroker.AuthorizeAsync(GoogleClientSecrets.Load(stream).Secrets, _scopes, "user", ct, new FileDataStore(credPath)).Result;

				Console.WriteLine("##INFO:: Credential file saved to: " + credPath);
			}

			return credential;
		}
		private SheetsService GetService()
		{
			var service = new SheetsService(new BaseClientService.Initializer()
			{
				HttpClientInitializer = GetCredential(),
				ApplicationName = _applicationName,
			});

			return service;
		}

		//>----------------------------------< UPDATEVALUES >-----------------------------------<//
		// Write specified values into the specified range of the spreadsheet. 
		// !! Previous content would be erased !!
		// > IN: 
		// (string) range - range where values will be written. Format should be: 
		//					"'sheet'!A1:B2"
		// (IList<IList<object>> values - values to be be written. Must be compliant with 
		//								specified range.
		//								For exemple, with range=A1:B2, then [[1,2],[3,4]] will
		//								set A1=1,B1=2,A2=3,B2=4
		// > OUT:
		//		TBD
		//>-------------------------------------------------------------------------------------<//
		public int UpdateValues(string range, IList<IList<object>> values)
		{
			SheetsService service = GetService();

			// How the input data should be interpreted
			SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum valueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;

			// Data to be input
			Apis.Sheets.v4.Data.ValueRange requestBody = new Apis.Sheets.v4.Data.ValueRange()
			{
				MajorDimension = "ROWS",
				Range = range,
				Values = values
			};

			// Make request
			SpreadsheetsResource.ValuesResource.UpdateRequest request = service.Spreadsheets.Values.Update(requestBody, _spreadsheetID, range);
			request.ValueInputOption = valueInputOption;

			Apis.Sheets.v4.Data.UpdateValuesResponse response = request.Execute();

			// Update result
			Console.WriteLine("##INFO::Update response:");
			Console.WriteLine(JsonConvert.SerializeObject(response));

			return 0;
		}

		//>----------------------------------< APPENDVALUES >-----------------------------------<//
		// Write specified values on the first empty row available in the spreadsheet. 
		// > IN: 
		// (string) range - range where values will be written, if a free space is found. 
		//					if not, a new row will be added.
		//					Format should be: "'sheet'!A1:B2"
		// (IList<IList<object>> values - values to be be written. Must be compliant with 
		//								specified range.
		//								For exemple, with range=A1:B2, then [[1,2],[3,4]] will
		//								set A1=1,B1=2,A2=3,B2=4
		// > OUT:
		//		TBD
		//>-------------------------------------------------------------------------------------<//
		public int AppendValues(string range, IList<IList<object>> values)
		{
			SheetsService service = GetService();

			// How the input data should be interpreted = raw unformated text
			SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum valueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.RAW;

			// How the input data should be inserted = insert new row
			SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum insertDataOption = SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum.INSERTROWS;

			Apis.Sheets.v4.Data.ValueRange requestBody = new Apis.Sheets.v4.Data.ValueRange()
			{
				MajorDimension = "ROWS",
				Range = range,
				Values = values
			};

			// Make request
			SpreadsheetsResource.ValuesResource.AppendRequest request = service.Spreadsheets.Values.Append(requestBody, _spreadsheetID, range);
			request.ValueInputOption = valueInputOption;
			request.InsertDataOption = insertDataOption;

			Apis.Sheets.v4.Data.AppendValuesResponse response = request.Execute();

			// Append result
			Console.WriteLine("##INFO::Append response:");
			Console.WriteLine(JsonConvert.SerializeObject(response));

			return 0;
		}

		//>----------------------------------< GETVALUESRANGE >---------------------------------<//
		// Extract data from the given range. 
		// > IN: 
		// (string) range - range where values will be get.
		//					Format should be: "'sheet'!A1:B2"
		// > OUT:
		//	(ValueRange) -	data returned as ValueRange object.
		//					See https://goo.gl/gjL6p8 for more infos about ValueRange objects.
		//>-------------------------------------------------------------------------------------<//
		public ValueRange GetValuesRange(string range)
		{
			SheetsService service = GetService();

			// How outputed datas should be formatted = raw unformated text
			SpreadsheetsResource.ValuesResource.GetRequest.ValueRenderOptionEnum valueOutputOption = SpreadsheetsResource.ValuesResource.GetRequest.ValueRenderOptionEnum.UNFORMATTEDVALUE;

			// Make request
			SpreadsheetsResource.ValuesResource.GetRequest request = service.Spreadsheets.Values.Get(_spreadsheetID, range);
			request.ValueRenderOption = valueOutputOption;
			ValueRange result = request.Execute();

			if ((result.Values != null) && (result.Values.Count > 0))
			{
				Console.WriteLine("##INFO::{0} row(s) found", result.Values.Count);
				return result;
			}
			else
			{
				Console.WriteLine("##INFO::No data in range {0}", range);
				return null;
			}
		}

		//>----------------------------------< FindRowByValue >---------------------------------<//
		// Search for the passed value in the passed range. 
		// > IN: 
		// (string) range -	range where value will be searched for.
		//					Format should be: "'sheet'!A1:B2"
		// (object) tofind - Value to search for.
		// > OUT:
		//	(int32) - row where the value has been found. Return 0 if not found.	 
		//>-------------------------------------------------------------------------------------<//
		public int FindRowByValue(string range, object tofind)
		{
			int i = 0;
			int minrow = 1;

			// Extract rows from range
			GroupCollection groups = Regex.Match(range, @"(?<=[A-Z])(\d+)(?=[:]|$)").Groups;

			// Get the first row of the range
			if (groups.Count > 1)
			{
				minrow = Math.Min(Int32.Parse(groups[0].Value), Int32.Parse(groups[1].Value));
			}

			ValueRange sheet = GetValuesRange(range);
			if (sheet != null)
			{
				// Search for the value to find
				foreach (var row in sheet.Values)
				{
					foreach (var cell in row)
					{
						if (cell.ToString() == tofind.ToString())
						{
							// Return row where the value has been found
							return i + minrow;
						}
					}
					i++;
				}
			}

			return 0; ;
		}
	}
}
