﻿using System;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace STDeviceManager
{
	/// <summary>
	/// Logique d'interaction pour UCADBExtract.xaml
	/// </summary>
	public partial class UCADBExtract : UserControl
	{
		public UCADBExtract(UCSetup setup)
		{
			InitializeComponent();
			_setup = setup;
			(Resources["LoadingDot"] as Storyboard).Begin();

			_bw = new BackgroundWorker()
			{
				WorkerReportsProgress = false,
				WorkerSupportsCancellation = true
			};
			_bw.DoWork += new DoWorkEventHandler(bw_DoWork);
			_bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
			_bw.RunWorkerAsync();
		}

		private UCSetup _setup;
		private BackgroundWorker _bw;

		private void bw_DoWork(object sender, DoWorkEventArgs e)
		{
			ZipFile.ExtractToDirectory(_setup.InstallPath + @"\adb.zip", _setup.InstallPath);
		}
		private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			File.Delete(_setup.InstallPath + @"\adb.zip");
			if (e.Error != null)
			{
				System.Windows.MessageBox.Show(e.Error.Message, "Error");
				_setup.Extracted = false;
			}
			else
			{
				_setup.Extracted = true;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			(Resources["SubContentFadeEffect"] as Storyboard).Begin();
		}
	}
}
