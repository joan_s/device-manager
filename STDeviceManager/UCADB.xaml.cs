﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace STDeviceManager
{
	/// <summary>
	/// Logique d'interaction pour UCADB.xaml
	/// </summary>
	public partial class UCADB : UserControl
	{
		public UCADB(UCSetup setup)
		{
			InitializeComponent();
			_setup = setup;
		}

		private UCSetup _setup;

		private void PathButton_Click(object sender, RoutedEventArgs e)
		{
			if(Settings.ADBAskForPath())
			{
				_setup.Success = true;
				var window = Window.GetWindow(this);
				window.Close();
			}
		}
		private void InstallButton_Click(object sender, RoutedEventArgs e)
		{
			_setup.StartDownload();
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			(Resources["SubContentFadeEffect"] as Storyboard).Begin();
		}
	}
}
