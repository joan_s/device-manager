﻿using System.Threading;
using System.Windows;
using System.Windows.Media.Animation;

namespace STDeviceManager
{
	/// <summary>
	/// Logique d'interaction pour UCSettings.xaml
	/// </summary>
	public partial class UCSettings : System.Windows.Controls.UserControl
	{
		public UCSettings()
		{
			InitializeComponent();

			DataContext = STDAApplicationContext.CurrentContext;
		}

		//>----------------------------------------------------------------------------------------< SETTINGS EVENTS >
		private void AdbLocation_Click(object sender, RoutedEventArgs e)
		{
			Settings.ADBConfiguration();
		}
		private void ApplyChanges_Click(object sender, RoutedEventArgs e)
		{
			if (Settings.SetChanges(SSRange.Text, SSID.Text, DisplayName.Text))
			{
				(Resources["SaveFadeEffect"] as Storyboard).Begin();
			}
		}
	}
}
