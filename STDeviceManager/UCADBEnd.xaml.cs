﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace STDeviceManager
{
	/// <summary>
	/// Logique d'interaction pour UCADBEnd.xaml
	/// </summary>
	public partial class UCADBEnd : UserControl
	{
		public UCADBEnd(UCSetup setup)
		{
			InitializeComponent();
			_setup = setup;

			EndInstall();
		}

		private UCSetup _setup;

		private void EndInstall()
		{
			if (_setup.Downloaded && _setup.Extracted)
			{
				if (Settings.ADBSetPath(Path.Combine(_setup.InstallPath, "platform-tools")))
				{
					SetSuccessMessage();
					_setup.Success = true;
				}
				else
				{
					SetFailedMessage();
					_setup.Success = false;
				}
			}
			else
			{
				SetFailedMessage();
				_setup.Success = false;
			}
		}

		private void SetSuccessMessage()
		{
			ResultTitle.Text = "Setup Complete";
			ResultDescription.Text = "Android Debug Bridge has been installed.";
		}
		private void SetFailedMessage()
		{
			ResultTitle.Text = "Setup Failed";
			ResultDescription.Text = "Android Debug Bridge installation has failed.";
		}

		private void CloseButton_Click(object sender, RoutedEventArgs e)
		{
			var window = Window.GetWindow(this);
			window.Close();
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			(Resources["SubContentFadeEffect"] as Storyboard).Begin();
		}
	}
}
