﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace STDeviceManager
{
	public class SysTrayIcon
	{
		private System.Windows.Forms.NotifyIcon _notifyIcon = null;

		//> PROPERTIES
		public NotifyIcon GetNotifyIcon
		{
			get
			{
				return _notifyIcon;
			}
		}
		public bool Visible
		{
			get
			{
				return _notifyIcon.Visible;
			}
			set
			{
				_notifyIcon.Visible = value;
			}
		}

		//> CONSTRUCTOR
		public SysTrayIcon()
		{
			_notifyIcon = new NotifyIcon();
			_notifyIcon.ContextMenuStrip = new ContextMenuStrip();
			_notifyIcon.ContextMenuStrip.Opening += this.ContextMenu_Opening;
			_notifyIcon.Click += new EventHandler(notifyIcon_Click);
			_notifyIcon.DoubleClick += new EventHandler(notifyIcon_DoubleClick);
			_notifyIcon.Icon = Properties.Resources.Icon__BW;
			_notifyIcon.Visible = true;

			// Register to the daemon state event
			STDAApplicationContext.CurrentContext.Daemon.PropertyChanged += new PropertyChangedEventHandler(reportDaemonState);
		}

		//> ICON CLICK EVENTS
		private void notifyIcon_Click(object sender, EventArgs e)
		{
		}
		private void notifyIcon_DoubleClick(object sender, EventArgs e)
		{
		}

		private void reportDaemonState(object sender, PropertyChangedEventArgs e)
		{
			if (STDAApplicationContext.CurrentContext.Daemon.IsStarted)
			{
				_notifyIcon.Icon = Properties.Resources.Icon_;
			}
			else
			{
				_notifyIcon.Icon = Properties.Resources.Icon__BW;
			}
		}

		//>----------------------------------------------------------------------------------------------< CONTEXT MENU >
		//> MENU ITEMS
		private ToolStripMenuItem _exitMenuItem;
		private ToolStripMenuItem _settingsMenuItem;
		private ToolStripMenuItem _updateMenuItem;

		//> MENU OPENING METHOD
		private void ContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel = false;

			if (_notifyIcon.ContextMenuStrip.Items.Count == 0)
			{
				_updateMenuItem = new ToolStripMenuItem("&Update", null, new EventHandler(updateItem_Click));
				_notifyIcon.ContextMenuStrip.Items.Add(_updateMenuItem);
				_updateMenuItem.Enabled = false;

				_settingsMenuItem = new ToolStripMenuItem("&Settings", null, new EventHandler(settingsItem_Click));
				_notifyIcon.ContextMenuStrip.Items.Add(_settingsMenuItem);

				_notifyIcon.ContextMenuStrip.Items.Add(new ToolStripSeparator());

				_exitMenuItem = new ToolStripMenuItem("&Exit", null, new EventHandler(exitItem_Click));
				_notifyIcon.ContextMenuStrip.Items.Add(_exitMenuItem);
			}
		}

		//> MENU ITEMS CLICK EVENTS
		private void updateItem_Click(object sender, EventArgs e)
		{
		}
		private void settingsItem_Click(object sender, EventArgs e)
		{
			WindowMain settings = new WindowMain(new UCSettings());
			System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(settings);
			settings.Show();
		}
		private void exitItem_Click(object sender, EventArgs e)
		{
			STDAApplicationContext.CurrentContext.Exit();
		}
	}
}
