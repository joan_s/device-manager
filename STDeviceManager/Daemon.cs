﻿using Android;
using Apple;
using Common;
using Google;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace STDeviceManager
{
	/// <summary>
	/// Entry point for the daemon.
	/// 
	/// </summary>
	public class Daemon : INotifyPropertyChanged
	{
		//>----------------------------------------------------------------------------------------------< CONSTRUCTOR >
		public Daemon()
		{
		}

		//>----------------------------------------------------------------------------------------------< LOCAL MEMBERS >
		private string _sheetrange;
		private string _spreadsheetID;
		private string _displayName;
		private bool _useDisplayName = Properties.Settings.Default.DisplayUserNameUse;

		private bool _alive = false;
		private bool _running = false;

		private string _applicationName = Properties.Settings.Default.ApplicationName;

		private List<Device> _connectedDevices = new List<Device>();
		private IGSheet _spreadsheet;
		private Thread _t;

		//>----------------------------------------------------------------------------------------------< PROPERTIES >
		//> INTERFACE EVENTS
		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(string propertyname)
		{
			var propertychanged = PropertyChanged;
			if (propertychanged != null)
			{
				propertychanged(this, new PropertyChangedEventArgs(propertyname));
			}
		}

		//> PROPERTIES
		public bool IsStarted
		{
			get
			{
				return _running;
			}
			set
			{
				if (value)
				{
					Thread t = new Thread(() =>
					{
						_running = value;
						OnPropertyChanged("IsStarted");

						Console.WriteLine("##INFO::Trying to start daemon");
						if (Init())
						{
							Start();
							Console.WriteLine("##INFO::Daemon started");
						}
						else
						{
							_running = !value;
							OnPropertyChanged("IsStarted");
							Console.WriteLine("##WARN::Failed to start daemon");
						}
					});
					t.Start();
				}
				else
				{
					_running = value;
					OnPropertyChanged("IsStarted");
					Console.WriteLine("##INFO::Daemon stopped");
				}
			}
		}
		public bool IsAlive
		{
			get
			{
				return _alive;
			}
		}
		public bool UseDisplayName
		{
			get
			{
				return _useDisplayName;
			}
			set
			{
				_useDisplayName = value;
				Properties.Settings.Default.DisplayUserNameUse = value;
				Properties.Settings.Default.Save();
				OnPropertyChanged("UseDisplayName");
			}
		}

		//>----------------------------------------------------------------------------------------------< METHODS >
		private int Start()
		{
			// Only one instance of the daemon is allowed.
			if (!_alive)
			{
				_t = new Thread(() =>
				{
					_alive = true;

					Runnable();

					_alive = false;
				});
				_t.Start();
			}

			return 0;
		}
		private bool Init()
		{
			_sheetrange = Properties.Settings.Default.SheetRange;
			_displayName = Properties.Settings.Default.DisplayUserName;
#if DEBUG
			_spreadsheetID = Properties.Settings.Default.SpreadsheetID_DEBUG;
#else
			_spreadsheetID = Properties.Settings.Default.SpreadsheetID;
#endif
			_spreadsheet = new IGSheet(_spreadsheetID, _applicationName);

			try
			{
				AndroidDebugBridge.RestartServer();
			}
			catch (Exception e)
			{
				System.Windows.Forms.MessageBox.Show(e.Message, "Error");
				return false;
			}

			return true;
		}

		public bool ADBValidation()
		{
			try
			{
				if (AndroidDebugBridge.Check())
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch
			{
				return false;
			}
		}
		private List<object> SerializeDevice(Device d, bool connected)
		{
			string time = DateTime.Now.ToString(new CultureInfo("en-US"));              // System date
			string user;
			string status;

			if (_useDisplayName)
			{
				user = Environment.MachineName + "\\" + _displayName;
			}
			else
			{
				user = Environment.MachineName + "\\" + Environment.UserName;
			}

			if (connected)
			{
				status = "Connected";
			}
			else
			{
				status = "Disconnected";
			}

			List<object> data = new List<object>()
			{
				time,
				user,
				status,
				d._Manufacturer,
				d._MarketName,
				d._OS,
				d._OSVersion,
				d._UDID
			};

			return data;
		}
		private void UpdateDeviceStatus(Device d, bool connected = true)
		{
			IList<IList<object>> devicesdata = new List<IList<object>>()
			{
				SerializeDevice(d, connected)
			};

			int row = _spreadsheet.FindRowByValue(_sheetrange, d._UDID);
			if (row != 0)
			{
				// Generate range from complete sheet's range and found row
				string range = Regex.Replace(_sheetrange, @"(?<=[(!|:)])([A-Z])([\d+]*)(?=[:]|$)", "${1}" + row.ToString());
				Console.WriteLine("##INFO::Update range: {0}", range);

				// Update found row
				_spreadsheet.UpdateValues(range, devicesdata);
			}
			else
			{
				// Device not found, so append it
				_spreadsheet.AppendValues(_sheetrange, devicesdata);
			}
		}

		public void ReInit()
		{
			if (IsStarted)
			{
				IsStarted = false;
				IsStarted = true;
			}
			else
			{
				Init();
			}
		}

		//>----------------------------------------------------------------------------------------------< WORKER THREAD >
		private void Runnable()
		{
			while (_running)
			{
				List<Device> newlist = new List<Device>();
				List<Device> disconnectedlist = new List<Device>();
				disconnectedlist.AddRange(_connectedDevices);

				AndroidDebugBridge.GetDevices(newlist);
				AppleIO.GetDevices(newlist);

				// Filter new and disconnected lists
				foreach (Device d in _connectedDevices)
				{
					// Remove all know devices from the buffer
					if (newlist.RemoveAll(x => x._ID == d._ID) > 0)
					{
						// Remove still connected devices from the disconnected list
						disconnectedlist.RemoveAll(x => x._ID == d._ID);
					}
				}

				// Handle newly connected devices
				if (newlist.Count > 0)
				{
					// Serialize and update data from all newly connected devices
					foreach (Device d in newlist)
					{
						_connectedDevices.Add(d);
						try
						{
							UpdateDeviceStatus(d);
						}
						catch (Exception e)
						{
							System.Windows.MessageBox.Show(@"An error occured while trying to update the spreadsheet: " + e.Message, "Error");
							IsStarted = false;
						}
					}
				}

				// Handle disconnected devices
				if (disconnectedlist.Count > 0)
				{
					foreach (Device d in disconnectedlist)
					{
						_connectedDevices.RemoveAll(x => x._ID == d._ID);
						try
						{
							UpdateDeviceStatus(d, false);
						}
						catch (Exception e)
						{
							System.Windows.MessageBox.Show(@"An error occured while trying to update the spreadsheet: " + e.Message, "Error");
							IsStarted = false;
						}
					}
				}

				newlist = null;
				disconnectedlist = null;

				Thread.Sleep(1000);
			}
		}
	}
}
