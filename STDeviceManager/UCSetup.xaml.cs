﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace STDeviceManager
{
	/// <summary>
	/// Logique d'interaction pour UCADBSetup.xaml
	/// </summary>
	public partial class UCSetup : UserControl, INotifyPropertyChanged
	{
		public UCSetup()
		{
			InitializeComponent();
			Initialize();

			UCSubContent.Content = new UCADB(this);
		}

		//> INTERFACE EVENTS
		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(string propertyname)
		{
			var propertychanged = PropertyChanged;
			if (propertychanged != null)
			{
				propertychanged(this, new PropertyChangedEventArgs(propertyname));
			}
		}

		private void Initialize()
		{
			_Installpath = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Properties.Settings.Default.ADBInstallDir);
			_downloaded = false;
			_extracted = false;
			_success = false;
		}

		private string _Installpath;
		public string InstallPath
		{
			get
			{
				return _Installpath;
			}
		}

		public void StartDownload()
		{
			UCSubContent.Content = new UCADBDownload(this);
		}
		private void StartExtraction()
		{
			UCSubContent.Content = new UCADBExtract(this);
		}
		private void StartFinish()
		{
			UCSubContent.Content = new UCADBEnd(this);
		}

		private bool _downloaded;
		public bool Downloaded
		{
			get
			{
				return _downloaded;
			}
			set
			{
				_downloaded = value;

				if(value)
				{
					StartExtraction();
				}
				else
				{
					StartFinish();
				}
			}
		}

		private bool _extracted;
		public bool Extracted
		{
			get
			{
				return _extracted;
			}
			set
			{
				_extracted = value;
				StartFinish();
			}
		}

		private bool _success;
		public bool Success
		{
			get
			{
				return _success;
			}
			set
			{
				_success = value;
			}
		}
	}
}
